package main

import (
	"html/template"
	"log"
	"net/http"
	"regexp"
	"strings"
)

type Page struct {
	tmp string
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("%s requested /\n", r.RemoteAddr)
	renderTemplate(w, "index", nil)
}

func headerHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("%s requested /header\n", r.RemoteAddr)

	//response := HeaderResponse{Username: getUsername(r)}
	err := templates.ExecuteTemplate(w, "header.html", nil)
	if err != nil {
		log.Printf("%s error processing request: %s", r.RemoteAddr, err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func mediaHandler(w http.ResponseWriter, r *http.Request) {
	name := r.FormValue("name")

	log.Printf("%s requested media id %s\n", r.RemoteAddr, name)

	if strings.Contains(name, "..") || strings.Contains(name, "/") || strings.Contains(name, "\\") {
		log.Printf("%s may have tried an LFI!", r.RemoteAddr)
		http.Error(w, "Attempt logged and administrator notified. :)", http.StatusInternalServerError)
		return
	}

	http.ServeFile(w, r, "media/"+name)
}

var templates = template.Must(template.ParseFiles("templates/index.html", "templates/header.html", "templates/database.html", "templates/database_items.html", "templates/database_spells.html", "templates/database_prayers.html", "templates/database_npcs.html", "templates/database_objects.html", "templates/database_wallobjects.html",
	"templates/highscores_index.html", "templates/highscores.html", "templates/how_to_play.html", "templates/statistics_item.html"))

func renderTemplate(w http.ResponseWriter, tmpl string, p *Page) {
	err := templates.ExecuteTemplate(w, tmpl+".html", p)
	if err != nil {
		log.Printf("error processing template: %s", err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

var validPath = regexp.MustCompile("^(/)|(/index(.*))|(/media(.*))|(/header(.*))|(/database)|(/database_items)|(/database_spells)|(/database_prayers)|(/database_npcs)|(/database_objects)|(/database_wallobjects)|(/highscores*)|(/statistics*)$")

func makeHandler(fn func(http.ResponseWriter, *http.Request)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		m := validPath.FindStringSubmatch(r.URL.Path)
		if m == nil {
			log.Printf("%s requested and received 404", r.RemoteAddr)
			http.NotFound(w, r)
			return
		}
		fn(w, r)
	}
}

func main() {

	log.Printf("Configuration:\n")
	log.Printf("Opening database...\n")

	log.Printf("Setting up handlers...\n")
	http.HandleFunc("/", makeHandler(indexHandler))
	http.HandleFunc("/media", makeHandler(mediaHandler))
	http.HandleFunc("/header", makeHandler(headerHandler))

	http.HandleFunc("/how_to_play", makeHandler(howToPlayHandler))

	http.HandleFunc("/highscores_index", makeHandler(highscoresIndexHandler))
	http.HandleFunc("/highscores_data", makeHandler(highscoresDataHandler))
	http.HandleFunc("/highscores_search", makeHandler(highscoresSearchHandler))
	http.HandleFunc("/highscores_player_data", makeHandler(highscoresPlayerDataHandler))
	http.HandleFunc("/highscores_rares_data", makeHandler(highscoresRaresDataHandler))
	http.HandleFunc("/highscores_skill_data", makeHandler(highscoresSkillDataHandler))
	http.HandleFunc("/highscores_player_npc_kills_data", makeHandler(highscoresPlayerNpcKillsDataHandler))
	http.HandleFunc("/highscores_npc_kills_data", makeHandler(highscoresNpcKillsDataHandler))

	http.HandleFunc("/database", makeHandler(databaseHandler))
	http.HandleFunc("/database_items", makeHandler(databaseItemsHandler))
	http.HandleFunc("/database_spells", makeHandler(databaseSpellsHandler))
	http.HandleFunc("/database_prayers", makeHandler(databasePrayersHandler))
	http.HandleFunc("/database_npcs", makeHandler(databaseNpcsHandler))
	http.HandleFunc("/database_objects", makeHandler(databaseObjectsHandler))
	http.HandleFunc("/database_wallobjects", makeHandler(databaseWallObjectsHandler))

	http.HandleFunc("/player_count", makeHandler(playerCountHandler))
	http.HandleFunc("/statistics_item", makeHandler(statisticsItemPageHandler))
	http.HandleFunc("/statistics_item_data", makeHandler(statisticsItemDataHandler))

	log.Printf("Starting server...")
	//	log.Fatal(http.ListenAndServe(":8081", nil))
	log.Fatal(http.ListenAndServeTLS(":"+HTTPS_PORT, "cert.pem", "privkey.pem", nil))
}
