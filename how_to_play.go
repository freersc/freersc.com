package main

import (
	"log"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
)

func howToPlayHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("%s requested /how_to_play\n", r.RemoteAddr)

	err := templates.ExecuteTemplate(w, "how_to_play.html", nil)
	if err != nil {
		log.Printf("%s error processing request: %s", r.RemoteAddr, err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
