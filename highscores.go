package main

import (
	"database/sql"
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"strconv"

	_ "github.com/go-sql-driver/mysql"
)

type HighScoresMasterListObject struct {
	Username        string `json:"username"`
	TotalLevel      int    `json:"totalLevel"`
	TotalExperience int    `json:"totalExperience"`
}

type HighScoresMasterListResponse struct {
	HighScoresList []HighScoresMasterListObject `json:"highScores"`
}

type HighScoresUsernameListObject struct {
	Username string `json:"username"`
	Server   string `json:"server"`
}

type HighScoresUsernameListResponse struct {
	HighScoresUsernameList []HighScoresUsernameListObject `json:"users"`
}

type HighScorePlayerObject struct {
	TotalLevel      int `json:"totalLevel"`
	TotalExperience int `json:"totalExperience"`

	AttackLevel     int `json:"attackLevel"`
	DefenseLevel    int `json:"defenseLevel"`
	StrengthLevel   int `json:"strengthLevel"`
	HitsLevel       int `json:"hitsLevel"`
	RangedLevel     int `json:"rangedLevel"`
	PrayerLevel     int `json:"prayerLevel"`
	MagicLevel      int `json:"magicLevel"`
	CookingLevel    int `json:"cookingLevel"`
	WoodcutLevel    int `json:"woodcutLevel"`
	FletchingLevel  int `json:"fletchingLevel"`
	FishingLevel    int `json:"fishingLevel"`
	FiremakingLevel int `json:"firemakingLevel"`
	CraftingLevel   int `json:"craftingLevel"`
	SmithingLevel   int `json:"smithingLevel"`
	MiningLevel     int `json:"miningLevel"`
	HerblawLevel    int `json:"herblawLevel"`
	AgilityLevel    int `json:"agilityLevel"`
	ThievingLevel   int `json:"thievingLevel"`

	AttackExperience     int `json:"attackExperience"`
	DefenseExperience    int `json:"defenseExperience"`
	StrengthExperience   int `json:"strengthExperience"`
	HitsExperience       int `json:"hitsExperience"`
	RangedExperience     int `json:"rangedExperience"`
	PrayerExperience     int `json:"prayerExperience"`
	MagicExperience      int `json:"magicExperience"`
	CookingExperience    int `json:"cookingExperience"`
	WoodcutExperience    int `json:"woodcutExperience"`
	FletchingExperience  int `json:"fletchingExperience"`
	FishingExperience    int `json:"fishingExperience"`
	FiremakingExperience int `json:"firemakingExperience"`
	CraftingExperience   int `json:"craftingExperience"`
	SmithingExperience   int `json:"smithingExperience"`
	MiningExperience     int `json:"miningExperience"`
	HerblawExperience    int `json:"herblawExperience"`
	AgilityExperience    int `json:"agilityExperience"`
	ThievingExperience   int `json:"thievingExperience"`

	WildernessKills  int `json:"wildernessKills"`
	WildernessDeaths int `json:"wildernessDeaths"`
	NPCKills         int `json:"npcKills"`
	QuestPoints      int `json:"questPoints"`
}

type HighScoresRaresListObject struct {
	Username   string `json:"username"`
	RaresCount int    `json:"raresCount"`
}

type HighScoresRaresListResponse struct {
	HighScoresList []HighScoresRaresListObject `json:"highScores"`
}

type HighScoresSkillListObject struct {
	Username   string `json:"username"`
	Level      int    `json:"level"`
	Experience int    `json:"experience"`
}

type HighScoresSkillListResponse struct {
	HighScoresList []HighScoresSkillListObject `json:"highScores"`
}

type HighScoresPlayerNpcKillsListObject struct {
	NpcName string `json:"npcName"`
	Kills   int    `json:"kills"`
}

type HighScoresPlayerNpcKillsListResponse struct {
	NpcKillsList []HighScoresPlayerNpcKillsListObject `json:"npcKillsList"`
}

type HighScoresNpcKillsListObject struct {
	Username string `json:"username"`
	Kills    int    `json:"kills"`
}

type HighScoresNpcKillsListResponse struct {
	NpcKillsList []HighScoresNpcKillsListObject `json:"npcKillsList"`
}

var npcNames []string = []string{"Unicorn", "Bob", "Sheep", "Chicken", "Goblin", "Hans", "cow", "cook", "Bear", "Priest", "Urhney", "Man", "Bartender", "Camel", "Gypsy", "Ghost", "Sir Prysin", "Traiborn the wizard", "Captain Rovin", "Rat", "Reldo", "mugger", "Lesser Demon", "Giant Spider", "Man", "Jonny the beard", "Baraek", "Katrine", "Tramp", "Rat", "Romeo", "Juliet", "Father Lawrence", "Apothecary", "spider", "Delrith", "Veronica", "Weaponsmaster", "Professor Oddenstein", "Curator", "skeleton", "zombie", "king", "Giant bat", "Bartender", "skeleton", "skeleton", "Rat", "Horvik the Armourer", "Bear", "skeleton", "Shopkeeper", "zombie", "Ghost", "Aubury", "Shopkeeper", "shopkeeper", "Darkwizard", "lowe", "Thessalia", "Darkwizard", "Giant", "Goblin", "farmer", "Thief", "Guard", "Black Knight", "Hobgoblin", "zombie", "Zaff", "Scorpion", "silk trader", "Man", "Guide", "Giant Spider", "Peksa", "Barbarian", "Fred the farmer", "Gunthor the Brave", "Witch", "Ghost", "Wizard", "Shop Assistant", "Shop Assistant", "Zeke", "Louie Legs", "Warrior", "Shopkeeper", "Shop Assistant", "Highwayman", "Kebab Seller", "Chicken", "Ernest", "Monk", "Dwarf", "Banker", "Count Draynor", "Morgan", "Dr Harlow", "Deadly Red spider", "Guard", "Cassie", "White Knight", "Ranael", "Moss Giant", "Shopkeeper", "Shop Assistant", "Witch", "Black Knight", "Greldo", "Sir Amik Varze", "Guildmaster", "Valaine", "Drogo", "Imp", "Flynn", "Wyson the gardener", "Wizard Mizgog", "Prince Ali", "Hassan", "Osman", "Joe", "Leela", "Lady Keli", "Ned", "Aggie", "Prince Ali", "Jailguard", "Redbeard Frank", "Wydin", "shop assistant", "Brian", "squire", "Head chef", "Thurgo", "Ice Giant", "King Scorpion", "Pirate", "Sir Vyvin", "Monk of Zamorak", "Monk of Zamorak", "Wayne", "Barmaid", "Dwarven shopkeeper", "Doric", "Shopkeeper", "Shop Assistant", "Guide", "Hetty", "Betty", "Bartender", "General wartface", "General Bentnoze", "Goblin", "Goblin", "Herquin", "Rommik", "Grum", "Ice warrior", "Warrior", "Thrander", "Border Guard", "Border Guard", "Customs Officer", "Luthas", "Zambo", "Captain Tobias", "Gerrant", "Shopkeeper", "Shop Assistant", "Seaman Lorris", "Seaman Thresnor", "Tanner", "Dommik", "Abbot Langley", "Thordur", "Brother Jered", "Rat", "Ghost", "skeleton", "zombie", "Lesser Demon", "Melzar the mad", "Scavvo", "Greater Demon", "Shopkeeper", "Shop Assistant", "Oziach", "Bear", "Black Knight", "chaos Dwarf", "dwarf", "Wormbrain", "Klarense", "Ned", "skeleton", "Dragon", "Oracle", "Duke of Lumbridge", "Dark Warrior", "Druid", "Red Dragon", "Blue Dragon", "Baby Blue Dragon", "Kaqemeex", "Sanfew", "Suit of armour", "Adventurer", "Adventurer", "Adventurer", "Adventurer", "Leprechaun", "Monk of entrana", "Monk of entrana", "zombie", "Monk of entrana", "tree spirit", "cow", "Irksol", "Fairy Lunderwin", "Jakut", "Doorman", "Fairy Shopkeeper", "Fairy Shop Assistant", "Fairy banker", "Giles", "Miles", "Niles", "Gaius", "Fairy Ladder attendant", "Jatix", "Master Crafter", "Bandit", "Noterazzo", "Bandit", "Fat Tony", "Donny the lad", "Black Heather", "Speedy Keith", "White wolf sentry", "Boy", "Rat", "Nora T Hag", "Grey wolf", "shapeshifter", "shapeshifter", "shapeshifter", "shapeshifter", "White wolf", "Pack leader", "Harry", "Thug", "Firebird", "Achetties", "Ice queen", "Grubor", "Trobert", "Garv", "guard", "Grip", "Alfonse the waiter", "Charlie the cook", "Guard Dog", "Ice spider", "Pirate", "Jailer", "Lord Darquarius", "Seth", "Banker", "Helemos", "Chaos Druid", "Poison Scorpion", "Velrak the explorer", "Sir Lancelot", "Sir Gawain", "King Arthur", "Sir Mordred", "Renegade knight", "Davon", "Bartender", "Arhein", "Morgan le faye", "Candlemaker", "lady", "lady", "lady", "Beggar", "Merlin", "Thrantax", "Hickton", "Black Demon", "Black Dragon", "Poison Spider", "Monk of Zamorak", "Hellhound", "Animated axe", "Black Unicorn", "Frincos", "Otherworldly being", "Owen", "Thormac the sorceror", "Seer", "Kharid Scorpion", "Kharid Scorpion", "Kharid Scorpion", "Barbarian guard", "Bartender", "man", "gem trader", "Dimintheis", "chef", "Hobgoblin", "Ogre", "Boot the Dwarf", "Wizard", "Chronozon", "Captain Barnaby", "Customs Official", "Man", "farmer", "Warrior", "Guard", "Knight", "Paladin", "Hero", "Baker", "silk merchant", "Fur trader", "silver merchant", "spice merchant", "gem merchant", "Zenesha", "Kangai Mau", "Wizard Cromperty", "RPDT employee", "Horacio", "Aemad", "Kortan", "zoo keeper", "Make over mage", "Bartender", "chuck", "Rogue", "Shadow spider", "Fire Giant", "Grandpa Jack", "Sinister stranger", "Bonzo", "Forester", "Morris", "Brother Omad", "Thief", "Head Thief", "Big Dave", "Joshua", "Mountain Dwarf", "Mountain Dwarf", "Brother Cedric", "Necromancer", "zombie", "Lucien", "The Fire warrior of lesarkus", "guardian of Armadyl", "guardian of Armadyl", "Lucien", "winelda", "Brother Kojo", "Dungeon Rat", "Master fisher", "Orven", "Padik", "Shopkeeper", "Lady servil", "Guard", "Guard", "Guard", "Guard", "Jeremy Servil", "Justin Servil", "fightslave joe", "fightslave kelvin", "local", "Khazard Bartender", "General Khazard", "Khazard Ogre", "Guard", "Khazard Scorpion", "hengrad", "Bouncer", "Stankers", "Docky", "Shopkeeper", "Fairy queen", "Merlin", "Crone", "High priest of entrana", "elkoy", "remsai", "bolkoy", "local gnome", "bolren", "Black Knight titan", "kalron", "brother Galahad", "tracker 1", "tracker 2", "tracker 3", "Khazard troop", "commander montai", "gnome troop", "khazard warlord", "Sir Percival", "Fisher king", "maiden", "Fisherman", "King Percival", "unhappy peasant", "happy peasant", "ceril", "butler", "carnillean guard", "Tribesman", "henryeta", "philipe", "clivet", "cult member", "Lord hazeel", "alomone", "Khazard commander", "claus", "1st plague sheep", "2nd plague sheep", "3rd plague sheep", "4th plague sheep", "Farmer brumty", "Doctor orbon", "Councillor Halgrive", "Edmond", "Citizen", "Citizen", "Citizen", "Citizen", "Citizen", "Jethick", "Mourner", "Mourner", "Ted Rehnison", "Martha Rehnison", "Billy Rehnison", "Milli Rehnison", "Alrena", "Mourner", "Clerk", "Carla", "Bravek", "Caroline", "Holgart", "Holgart", "Holgart", "kent", "bailey", "kennith", "Platform Fisherman", "Platform Fisherman", "Platform Fisherman", "Elena", "jinno", "Watto", "Recruiter", "Head mourner", "Almera", "hudon", "hadley", "Rat", "Combat instructor", "golrie", "Guide", "King Black Dragon", "cooking instructor", "fishing instructor", "financial advisor", "gerald", "mining instructor", "Elena", "Omart", "Bank assistant", "Jerico", "Kilron", "Guidor's wife", "Quest advisor", "chemist", "Mourner", "Mourner", "Wilderness guide", "Magic Instructor", "Mourner", "Community instructor", "boatman", "skeleton mage", "controls guide", "nurse sarah", "Tailor", "Mourner", "Guard", "Chemist", "Chancy", "Hops", "DeVinci", "Guidor", "Chancy", "Hops", "DeVinci", "king Lathas", "Head wizard", "Magic store owner", "Wizard Frumscone", "target practice zombie", "Trufitus", "Colonel Radick", "Soldier", "Bartender", "Jungle Spider", "Jiminua", "Jogre", "Guard", "Ogre", "Guard", "Guard", "shop keeper", "Bartender", "Frenita", "Ogre chieftan", "rometti", "Rashiliyia", "Blurberry", "Heckel funch", "Aluft Gianne", "Hudo glenfad", "Irena", "Mosol", "Gnome banker", "King Narnode Shareen", "UndeadOne", "Drucas", "tourist", "King Narnode Shareen", "Hazelmere", "Glough", "Shar", "Shantay", "charlie", "Gnome guard", "Gnome pilot", "Mehman", "Ana", "Chaos Druid warrior", "Gnome pilot", "Shipyard worker", "Shipyard worker", "Shipyard worker", "Shipyard foreman", "Shipyard foreman", "Gnome guard", "Femi", "Femi", "Anita", "Glough", "Salarin the twisted", "Black Demon", "Gnome pilot", "Gnome pilot", "Gnome pilot", "Gnome pilot", "Sigbert the Adventurer", "Yanille Watchman", "Tower guard", "Gnome Trainer", "Gnome Trainer", "Gnome Trainer", "Gnome Trainer", "Blurberry barman", "Gnome waiter", "Gnome guard", "Gnome child", "Earth warrior", "Gnome child", "Gnome child", "Gulluck", "Gunnjorn", "Zadimus", "Brimstail", "Gnome child", "Gnome local", "Gnome local", "Moss Giant", "Gnome Baller", "Goalie", "Gnome Baller", "Gnome Baller", "Gnome Baller", "Gnome Baller", "Referee", "Gnome Baller", "Gnome Baller", "Gnome Baller", "Gnome Baller", "Gnome Baller", "Gnome Baller", "Gnome Baller", "Gnome Baller", "Gnome Baller", "Cheerleader", "Cheerleader", "Nazastarool Zombie", "Nazastarool Skeleton", "Nazastarool Ghost", "Fernahei", "Jungle Banker", "Cart Driver", "Cart Driver", "Obli", "Kaleb", "Yohnus", "Serevel", "Yanni", "Official", "Koftik", "Koftik", "Koftik", "Koftik", "Blessed Vermen", "Blessed Spider", "Paladin", "Paladin", "slave", "slave", "slave", "slave", "slave", "slave", "slave", "Kalrag", "Niloof", "Kardia the Witch", "Souless", "Othainian", "Doomion", "Holthion", "Klank", "Iban", "Koftik", "Goblin guard", "Observatory Professor", "Ugthanki", "Observatory assistant", "Souless", "Dungeon spider", "Kamen", "Iban disciple", "Koftik", "Goblin", "Chadwell", "Professor", "San Tojalon", "Ghost", "Spirit of Scorpius", "Scorpion", "Dark Mage", "Mercenary", "Mercenary Captain", "Mercenary", "Mining Slave", "Watchtower wizard", "Ogre Shaman", "Skavid", "Ogre guard", "Ogre guard", "Ogre guard", "Skavid", "Skavid", "Og", "Grew", "Toban", "Gorad", "Ogre guard", "Yanille Watchman", "Ogre merchant", "Ogre trader", "Ogre trader", "Ogre trader", "Mercenary", "City Guard", "Mercenary", "Lawgof", "Dwarf", "lollk", "Skavid", "Ogre guard", "Nulodion", "Dwarf", "Al Shabim", "Bedabin Nomad", "Captain Siad", "Bedabin Nomad Guard", "Ogre citizen", "Rock of ages", "Ogre", "Skavid", "Skavid", "Skavid", "Draft Mercenary Guard", "Mining Cart Driver", "kolodion", "kolodion", "Gertrude", "Shilop", "Rowdy Guard", "Shantay Pass Guard", "Rowdy Slave", "Shantay Pass Guard", "Assistant", "Desert Wolf", "Workman", "Examiner", "Student", "Student", "Guide", "Student", "Archaeological expert", "civillian", "civillian", "civillian", "civillian", "Murphy", "Murphy", "Sir Radimus Erkle", "Legends Guild Guard", "Escaping Mining Slave", "Workman", "Murphy", "Echned Zekin", "Donovan the Handyman", "Pierre the Dog Handler", "Hobbes the Butler", "Louisa The Cook", "Mary The Maid", "Stanford The Gardener", "Guard", "Guard Dog", "Guard", "Man", "Anna Sinclair", "Bob Sinclair", "Carol Sinclair", "David Sinclair", "Elizabeth Sinclair", "Frank Sinclair", "kolodion", "kolodion", "kolodion", "kolodion", "Irvig Senay", "Ranalph Devere", "Poison Salesman", "Gujuo", "Jungle Forester", "Ungadulu", "Ungadulu", "Death Wing", "Nezikchened", "Dwarf Cannon engineer", "Dwarf commander", "Viyeldi", "Nurmof", "Fatigue expert", "Karamja Wolf", "Jungle Savage", "Oomlie Bird", "Sidney Smith", "Siegfried Erkle", "Tea seller", "Wilough", "Philop", "Kanel", "chamber guardian", "Sir Radimus Erkle", "Pit Scorpion", "Shadow Warrior", "Fionella", "Battle mage", "Battle mage", "Battle mage", "Gundai", "Lundail"}
var skillNames []string = []string{"attack", "defense", "strength", "hits", "ranged", "prayer", "magic", "cooking", "woodcut", "fletching", "fishing", "firemaking", "crafting", "smithing", "mining", "herblaw", "agility", "thieving"}
var experienceTable [100]int = [100]int{-1, 0, 83, 174, 276, 388, 512, 650, 801, 969, 1154, 1358, 1584, 1833, 2107, 2411, 2746, 3115, 3523, 3973, 4470, 5018, 5624, 6291, 7028, 7842, 8740, 9730, 10824, 12031, 13363, 14833, 16456, 18247, 20224, 22406, 24815, 27473, 30408, 33648, 37224, 41171, 45529, 50339, 55649, 61512, 67983, 75127, 83014, 91721, 101333, 111945, 123660, 136594, 150872, 166636, 184040, 203254, 224466, 247886, 273742, 302288, 333804, 368599, 407015, 449428, 496254, 547953, 605032, 668051, 737627, 814445, 899257, 992895, 1096278, 1210421, 1336443, 1475581, 1629200, 1798808, 1986068, 2192818, 2421087, 2673114, 2951373, 3258594, 3597792, 3972294, 4385776, 4842295, 5346332, 5902831, 6517253, 7195629, 7944614, 8771558, 9684577, 10692629, 11805606, 13034431}

func highscoresIndexHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("%s requested /highscores\n", r.RemoteAddr)

	err := templates.ExecuteTemplate(w, "highscores_index.html", nil)
	if err != nil {
		log.Printf("%s error processing request: %s", r.RemoteAddr, err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func connectToDatabase(connectionString string) (*sql.DB, error) {
	db, err := sql.Open("mysql", connectionString)

	if err != nil {
		return nil, err
	}

	db.SetConnMaxLifetime(5)
	db.SetMaxOpenConns(1)
	db.SetMaxIdleConns(1)

	return db, nil
}

func checkErrors(w http.ResponseWriter, r *http.Request, err error) bool {
	if err != nil {
		log.Printf("%s error processing request: %s", r.RemoteAddr, err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return true
	}

	return false
}

func convertExperienceToLevel(experience int) int {
	var level int = 0

	for i, xp := range experienceTable {
		if experience >= xp {
			level = i
		}
	}

	if experience == 1000 { //TODO: fix HP, would need to reference what the skill is inside the parameters, bleh.
		level = 10
	}

	return level
}

func convertNpcIdToName(npcId int) string {
	return npcNames[npcId]
}

func highscoresDataHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("%s requested /highscores_data\n", r.RemoteAddr)

	var databaseConnection *sql.DB
	var err error
	var serverType string
	var offset int
	var rows *sql.Rows
	var entries [20]HighScoresMasterListObject

	serverType = r.FormValue("server")
	offset, err = strconv.Atoi(r.FormValue("offset"))

	if err != nil {
		offset = 0
	}

	if serverType == "botting" {
		databaseConnection, err = connectToDatabase(DATABASE_CONNECTION_STRING_BOTTING)
	} else {
		databaseConnection, err = connectToDatabase(DATABASE_CONNECTION_STRING_LEGIT)
	}

	if checkErrors(w, r, err) {
		return
	}

	rows, err = databaseConnection.Query("SELECT players.username,players.skill_total,FLOOR(experience.attack/4+experience.defense/4+experience.strength/4+experience.hits/4+experience.ranged/4+experience.prayer/4+experience.magic/4+experience.cooking/4+experience.woodcut/4+experience.fletching/4+experience.fishing/4+experience.firemaking/4+experience.crafting/4+experience.smithing/4+experience.mining/4+experience.herblaw/4+experience.agility/4+experience.thieving/4) AS total_experience FROM players INNER JOIN experience ON experience.playerID=players.id "+
		"WHERE players.group_id = 10 "+
		"ORDER BY players.skill_total DESC, total_experience DESC "+
		"LIMIT 20 "+
		"OFFSET ?;", offset)

	if checkErrors(w, r, err) {
		return
	}
	defer rows.Close()

	index := 0
	for rows.Next() {
		var username string
		var totalLevel int
		var totalExperience int

		err = rows.Scan(&username, &totalLevel, &totalExperience)

		if checkErrors(w, r, err) {
			return
		}

		entries[index].Username = username
		entries[index].TotalLevel = totalLevel
		entries[index].TotalExperience = totalExperience

		index += 1
	}

	response := HighScoresMasterListResponse{
		HighScoresList: entries[0:index],
	}

	json.NewEncoder(w).Encode(response)
}

func highscoresSearchHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("%s requested /highscores_search\n", r.RemoteAddr)

	var err error
	var username string
	var entries [10]HighScoresUsernameListObject

	username = r.FormValue("username")

	databaseConnectionBotting, err := connectToDatabase(DATABASE_CONNECTION_STRING_BOTTING)

	if username == "" {
		err = errors.New("no username specified")
	}

	if checkErrors(w, r, err) {
		return
	}

	rows, err := databaseConnectionBotting.Query("SELECT username FROM players WHERE username LIKE ? LIMIT 5;", "%"+username+"%")
	if checkErrors(w, r, err) {
		return
	}
	defer rows.Close()

	index := 0
	for rows.Next() {
		var username string

		err = rows.Scan(&username)

		if checkErrors(w, r, err) {
			return
		}

		entries[index].Username = username
		entries[index].Server = "botting"

		index += 1
	}

	if checkErrors(w, r, err) {
		return
	}

	//repeat for legit
	databaseConnectionLegit, err := connectToDatabase(DATABASE_CONNECTION_STRING_LEGIT)
	if checkErrors(w, r, err) {
		return
	}

	rows, err = databaseConnectionLegit.Query("SELECT username FROM players WHERE username LIKE ? LIMIT 5;", username)

	if checkErrors(w, r, err) {
		return
	}

	defer rows.Close()

	for rows.Next() {
		var username string

		err = rows.Scan(&username)

		if checkErrors(w, r, err) {
			return
		}

		entries[index].Username = username
		entries[index].Server = "legit"

		index += 1
	}

	if checkErrors(w, r, err) {
		return
	}

	response := HighScoresUsernameListResponse{
		HighScoresUsernameList: entries[0:index],
	}

	json.NewEncoder(w).Encode(response)
}

// yes this function is ugly as hell. if you have a better way of doing it, please rewrite it.
func highscoresPlayerDataHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("%s requested /highscores_player_data\n", r.RemoteAddr)

	var databaseConnection *sql.DB
	var err error
	var serverType string
	var username string
	var row *sql.Row
	var entry HighScorePlayerObject

	serverType = r.FormValue("server")
	username = r.FormValue("username")

	if serverType == "botting" {
		databaseConnection, err = connectToDatabase(DATABASE_CONNECTION_STRING_BOTTING)
	} else {
		databaseConnection, err = connectToDatabase(DATABASE_CONNECTION_STRING_LEGIT)
	}

	if username == "" {
		err = errors.New("no username specified")
	}

	if checkErrors(w, r, err) {
		return
	}

	row = databaseConnection.QueryRow("SELECT players.skill_total,(FLOOR(experience.attack/4+experience.defense/4+experience.strength/4+experience.hits/4+experience.ranged/4+experience.prayer/4+experience.magic/4+experience.cooking/4+experience.woodcut/4+experience.fletching/4+experience.fishing/4+experience.firemaking/4+experience.crafting/4+experience.smithing/4+experience.mining/4+experience.herblaw/4+experience.agility/4+experience.thieving/4)),FLOOR(experience.attack/4),FLOOR(experience.defense/4),FLOOR(experience.strength/4),FLOOR(experience.hits/4),FLOOR(experience.ranged/4),FLOOR(experience.prayer/4),FLOOR(experience.magic/4),FLOOR(experience.cooking/4),FLOOR(experience.woodcut/4),FLOOR(experience.fletching/4),FLOOR(experience.fishing/4),FLOOR(experience.firemaking/4),FLOOR(experience.crafting/4),FLOOR(experience.smithing/4),FLOOR(experience.mining/4),FLOOR(experience.herblaw/4),FLOOR(experience.agility/4),FLOOR(experience.thieving/4), kills, deaths, npc_kills, quest_points FROM players INNER JOIN experience ON experience.playerID=players.id "+
		"WHERE username=?;", username)

	if checkErrors(w, r, err) {
		return
	}

	var totalLevel int
	var totalExperience int

	var attackLevel int
	var defenseLevel int
	var strengthLevel int
	var hitsLevel int
	var rangedLevel int
	var prayerLevel int
	var magicLevel int
	var cookingLevel int
	var woodcutLevel int
	var fletchingLevel int
	var fishingLevel int
	var firemakingLevel int
	var craftingLevel int
	var smithingLevel int
	var miningLevel int
	var herblawLevel int
	var agilityLevel int
	var thievingLevel int

	var attackExperience int
	var defenseExperience int
	var strengthExperience int
	var hitsExperience int
	var rangedExperience int
	var prayerExperience int
	var magicExperience int
	var cookingExperience int
	var woodcutExperience int
	var fletchingExperience int
	var fishingExperience int
	var firemakingExperience int
	var craftingExperience int
	var smithingExperience int
	var miningExperience int
	var herblawExperience int
	var agilityExperience int
	var thievingExperience int

	var wildernessKills int
	var wildernessDeaths int
	var npcKills int
	var questPoints int

	err = row.Scan(&totalLevel, &totalExperience, &attackExperience, &defenseExperience, &strengthExperience, &hitsExperience, &rangedExperience, &prayerExperience, &magicExperience, &cookingExperience, &woodcutExperience, &fletchingExperience, &fishingExperience, &firemakingExperience, &craftingExperience, &smithingExperience, &miningExperience, &herblawExperience, &agilityExperience, &thievingExperience, &wildernessKills, &wildernessDeaths, &npcKills, &questPoints)

	if checkErrors(w, r, err) {
		return
	}

	attackLevel = convertExperienceToLevel(attackExperience)
	defenseLevel = convertExperienceToLevel(defenseExperience)
	strengthLevel = convertExperienceToLevel(strengthExperience)
	hitsLevel = convertExperienceToLevel(hitsExperience)
	rangedLevel = convertExperienceToLevel(rangedExperience)
	prayerLevel = convertExperienceToLevel(prayerExperience)
	magicLevel = convertExperienceToLevel(magicExperience)
	cookingLevel = convertExperienceToLevel(cookingExperience)
	woodcutLevel = convertExperienceToLevel(woodcutExperience)
	fletchingLevel = convertExperienceToLevel(fletchingExperience)
	fishingLevel = convertExperienceToLevel(fishingExperience)
	firemakingLevel = convertExperienceToLevel(firemakingExperience)
	craftingLevel = convertExperienceToLevel(craftingExperience)
	smithingLevel = convertExperienceToLevel(smithingExperience)
	miningLevel = convertExperienceToLevel(miningExperience)
	herblawLevel = convertExperienceToLevel(herblawExperience)
	agilityLevel = convertExperienceToLevel(agilityExperience)
	thievingLevel = convertExperienceToLevel(thievingExperience)

	entry.TotalLevel = totalLevel
	entry.TotalExperience = totalExperience

	entry.AttackLevel = attackLevel
	entry.DefenseLevel = defenseLevel
	entry.StrengthLevel = strengthLevel
	entry.HitsLevel = hitsLevel
	entry.RangedLevel = rangedLevel
	entry.PrayerLevel = prayerLevel
	entry.MagicLevel = magicLevel
	entry.CookingLevel = cookingLevel
	entry.WoodcutLevel = woodcutLevel
	entry.FletchingLevel = fletchingLevel
	entry.FishingLevel = fishingLevel
	entry.FiremakingLevel = firemakingLevel
	entry.CraftingLevel = craftingLevel
	entry.SmithingLevel = smithingLevel
	entry.MiningLevel = miningLevel
	entry.HerblawLevel = herblawLevel
	entry.AgilityLevel = agilityLevel
	entry.ThievingLevel = thievingLevel

	entry.AttackExperience = attackExperience
	entry.DefenseExperience = defenseExperience
	entry.StrengthExperience = strengthExperience
	entry.HitsExperience = hitsExperience
	entry.RangedExperience = rangedExperience
	entry.PrayerExperience = prayerExperience
	entry.MagicExperience = magicExperience
	entry.CookingExperience = cookingExperience
	entry.WoodcutExperience = woodcutExperience
	entry.FletchingExperience = fletchingExperience
	entry.FishingExperience = fishingExperience
	entry.FiremakingExperience = firemakingExperience
	entry.CraftingExperience = craftingExperience
	entry.SmithingExperience = smithingExperience
	entry.MiningExperience = miningExperience
	entry.HerblawExperience = herblawExperience
	entry.AgilityExperience = agilityExperience
	entry.ThievingExperience = thievingExperience

	entry.WildernessKills = wildernessKills
	entry.WildernessDeaths = wildernessDeaths
	entry.NPCKills = npcKills
	entry.QuestPoints = questPoints

	json.NewEncoder(w).Encode(entry)
}

func highscoresRaresDataHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("%s requested /highscores_rares_data\n", r.RemoteAddr)

	var databaseConnection *sql.DB
	var err error
	var serverType string
	var offset int
	var rows *sql.Rows
	var entries [20]HighScoresRaresListObject

	serverType = r.FormValue("server")
	offset, err = strconv.Atoi(r.FormValue("offset"))

	if err != nil {
		offset = 0
	}

	if serverType == "botting" {
		databaseConnection, err = connectToDatabase(DATABASE_CONNECTION_STRING_BOTTING)
	} else {
		databaseConnection, err = connectToDatabase(DATABASE_CONNECTION_STRING_LEGIT)
	}

	if checkErrors(w, r, err) {
		return
	}

	rows, err = databaseConnection.Query("SELECT username,SUM(amount) FROM players INNER JOIN bank ON players.id=bank.playerID INNER JOIN itemstatuses ON itemstatuses.itemID=bank.itemID WHERE (catalogID=422 OR catalogID=387 OR catalogID=575 OR catalogID=677 OR catalogID=832 OR catalogID=828 OR catalogID=831 OR catalogID=971 OR catalogID=576 OR catalogID=577 OR catalogID=578 OR catalogID=579 OR catalogID=580 OR catalogID=581 OR catalogId=246) GROUP BY id ORDER BY sum(amount) DESC LIMIT 20 OFFSET ?;", offset)

	if checkErrors(w, r, err) {
		return
	}
	defer rows.Close()

	index := 0
	for rows.Next() {
		var username string
		var raresCount int

		err = rows.Scan(&username, &raresCount)

		if checkErrors(w, r, err) {
			return
		}

		entries[index].Username = username
		entries[index].RaresCount = raresCount

		index += 1
	}

	response := HighScoresRaresListResponse{
		HighScoresList: entries[0:index],
	}

	json.NewEncoder(w).Encode(response)
}

func highscoresSkillDataHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("%s requested /highscores_skill_data\n", r.RemoteAddr)

	var databaseConnection *sql.DB
	var err error
	var serverType string
	var skill string
	var offset int
	var rows *sql.Rows
	var entries [20]HighScoresSkillListObject

	serverType = r.FormValue("server")
	skill = r.FormValue("skill")
	offset, err = strconv.Atoi(r.FormValue("offset"))

	if err != nil {
		offset = 0
	}

	if serverType == "botting" {
		databaseConnection, err = connectToDatabase(DATABASE_CONNECTION_STRING_BOTTING)
	} else {
		databaseConnection, err = connectToDatabase(DATABASE_CONNECTION_STRING_LEGIT)
	}

	var isValidSkill = false
	for _, s := range skillNames {
		if skill == s {
			isValidSkill = true
			break
		}
	}

	if !isValidSkill {
		checkErrors(w, r, errors.New("Not a valid skill"))
		return
	}

	if checkErrors(w, r, err) {
		return
	}

	rows, err = databaseConnection.Query("SELECT players.username,FLOOR(experience."+skill+"/4) FROM players "+
		"INNER JOIN experience ON experience.playerID=players.id "+
		"WHERE players.group_id = 10 "+
		" ORDER BY FLOOR(experience."+skill+"/4) DESC "+
		"LIMIT 20 "+
		"OFFSET ?;", offset)

	if checkErrors(w, r, err) {
		return
	}
	defer rows.Close()

	index := 0
	for rows.Next() {
		var username string
		var experience int

		err = rows.Scan(&username, &experience)

		if checkErrors(w, r, err) {
			return
		}

		entries[index].Username = username
		entries[index].Level = convertExperienceToLevel(experience)
		entries[index].Experience = experience

		index += 1
	}

	response := HighScoresSkillListResponse{
		HighScoresList: entries[0:index],
	}

	json.NewEncoder(w).Encode(response)
}

func highscoresPlayerNpcKillsDataHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("%s requested /highscores_player_npc_kills_data\n", r.RemoteAddr)

	var databaseConnection *sql.DB
	var err error
	var serverType string
	var username string
	var rows *sql.Rows
	var entries [800]HighScoresPlayerNpcKillsListObject

	serverType = r.FormValue("server")
	username = r.FormValue("username")

	if serverType == "botting" {
		databaseConnection, err = connectToDatabase(DATABASE_CONNECTION_STRING_BOTTING)
	} else {
		databaseConnection, err = connectToDatabase(DATABASE_CONNECTION_STRING_LEGIT)
	}

	if checkErrors(w, r, err) {
		return
	}

	rows, err = databaseConnection.Query("SELECT npckills.npcID,npckills.killCount FROM players INNER JOIN npckills ON npckills.playerID=players.id WHERE username=? ORDER BY killCount DESC;", username)

	if checkErrors(w, r, err) {
		return
	}
	defer rows.Close()

	index := 0
	for rows.Next() {
		var npcId int
		var kills int

		err = rows.Scan(&npcId, &kills)

		if checkErrors(w, r, err) {
			return
		}

		entries[index].NpcName = convertNpcIdToName(npcId)
		entries[index].Kills = kills

		index += 1
	}

	response := HighScoresPlayerNpcKillsListResponse{
		NpcKillsList: entries[0:index],
	}

	json.NewEncoder(w).Encode(response)
}

func highscoresNpcKillsDataHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("%s requested /highscores_npc_kills_data\n", r.RemoteAddr)

	var databaseConnection *sql.DB
	var err error
	var serverType string
	var npcId string
	var rows *sql.Rows
	var entries [800]HighScoresNpcKillsListObject

	serverType = r.FormValue("server")
	npcId = r.FormValue("npcId")

	if serverType == "botting" {
		databaseConnection, err = connectToDatabase(DATABASE_CONNECTION_STRING_BOTTING)
	} else {
		databaseConnection, err = connectToDatabase(DATABASE_CONNECTION_STRING_LEGIT)
	}

	if checkErrors(w, r, err) {
		return
	}

	rows, err = databaseConnection.Query("SELECT players.username,killCount FROM npckills INNER JOIN players ON npckills.playerID=players.ID WHERE npcID = ? UNION SELECT \"--TOTAL--\",SUM(killCount) FROM npckills WHERE npcID=? ORDER BY killCount DESC;", npcId, npcId)

	if checkErrors(w, r, err) {
		return
	}
	defer rows.Close()

	index := 0
	for rows.Next() {
		var username string
		var kills int

		err = rows.Scan(&username, &kills)

		if checkErrors(w, r, err) {
			return
		}

		entries[index].Username = username
		entries[index].Kills = kills

		index += 1
	}

	response := HighScoresNpcKillsListResponse{
		NpcKillsList: entries[0:index],
	}

	json.NewEncoder(w).Encode(response)
}
