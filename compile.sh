#!/bin/sh
go build main.go config.go database.go highscores.go how_to_play.go statistics.go
echo "Now run this as root: /usr/sbin/setcap CAP_NET_BIND_SERVICE=+eip /path/to/exec"
