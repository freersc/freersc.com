package main

import (
	"log"
	"net/http"
)

func databaseHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("%s requested /database\n", r.RemoteAddr)

	err := templates.ExecuteTemplate(w, "database.html", nil)
	if err != nil {
		log.Printf("%s error processing request: %s", r.RemoteAddr, err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func databaseItemsHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("%s requested /database_items\n", r.RemoteAddr)

	err := templates.ExecuteTemplate(w, "database_items.html", nil)
	if err != nil {
		log.Printf("%s error processing request: %s", r.RemoteAddr, err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func databaseSpellsHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("%s requested /database_spells\n", r.RemoteAddr)

	err := templates.ExecuteTemplate(w, "database_spells.html", nil)
	if err != nil {
		log.Printf("%s error processing request: %s", r.RemoteAddr, err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func databasePrayersHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("%s requested /database_prayers\n", r.RemoteAddr)

	err := templates.ExecuteTemplate(w, "database_prayers.html", nil)
	if err != nil {
		log.Printf("%s error processing request: %s", r.RemoteAddr, err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func databaseNpcsHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("%s requested /database_npcs\n", r.RemoteAddr)

	err := templates.ExecuteTemplate(w, "database_npcs.html", nil)
	if err != nil {
		log.Printf("%s error processing request: %s", r.RemoteAddr, err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func databaseObjectsHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("%s requested /database_objects\n", r.RemoteAddr)

	err := templates.ExecuteTemplate(w, "database_objects.html", nil)
	if err != nil {
		log.Printf("%s error processing request: %s", r.RemoteAddr, err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func databaseWallObjectsHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("%s requested /database_wallobjects\n", r.RemoteAddr)

	err := templates.ExecuteTemplate(w, "database_wallobjects.html", nil)
	if err != nil {
		log.Printf("%s error processing request: %s", r.RemoteAddr, err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
